Nombre del dominio
----------------------------------------------------

* Utilizando el registro de prácticas pasadas se modifica de la sig. manera para que quede con el dominio <b>practica6.redes.mx</b>
<p>
	<img src="img/1.png"/>
</p>
* Ahora especificamos el puerto 8080 al dominio para acceder al sitio:
<p>
	<img src="img/2.png"/>
</p>
