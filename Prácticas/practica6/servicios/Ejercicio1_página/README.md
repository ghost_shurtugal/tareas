Montando la página y su base
----------------------------------------------------

## El proyecto web (hecho en netbeans) y la base de datos (hecha en mariadb) se anexan en este directorio.

* El proyecto es un pequeño Facebook con opciones muy básicas como crear un usuario, iniciar sesión, cerrar sesión, publicar cosas, eliminar, buscar usuarios y buscar publicaciones.

* Montamos la página web con su respectiva base de datos:
<p>
	<img src="img/1.png"/>
</p>
<p>
	<img src="img/2.png"/>
</p>
<p>
	<img src="img/3.png"/>
</p>
<p>
	<img src="img/4.png"/>
</p>
<p>
	<img src="img/5.png"/>
</p>
