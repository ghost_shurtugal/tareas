BASIC authentication
----------------------------------------------------

* Iniciamos sesión en la página de administración del servidor Glassfish:
<p>
	<img src="img/1.png"/>
</p>
* Vamos al menú de configurations->default-config->security->Realms y damos click en Realms, nos aparecerá el siguiente menú:
<p>
	<img src="img/2.png"/>
</p>
Damos click en el link de <b>file</b>.<br/>
* En el menú que aparecerá llenamos el campo de "Assign Groups" los valores de "admin, default", estos se ocuparán dentro de la aplicación en algunos pasos y al final damos en guardar:
<p>
	<img src="img/3.png"/>
</p>
* En la interfaz anterior presionamos el botón "Manage Users" para que nos de la opción de dar de alta a los usuarios con los que nos autentificaremos (para este caso hicimos al usuario <b>equipo7</b> con la contraseña <b>equipo7</b> en el grupo de admin):
<p>
	<img src="img/4.png"/>
</p>
* Damos guardar y nos aparecerá la siguiente pantalla para confirmar que todo salió bien.
<p>
	<img src="img/5.png"/>
</p>
* En el archivo <b>web.xml</b> de nuestro proyecto le damos los siguientes atributos de seguridad para que sólo al tener el rol de admin se puede acceder a cualquier recurso de la página (resultando en un error 403 en caso de que no se tenga ese rol), esto incluye métodos GET y POST.<br/>
También especificamos que utilizaremos el método <b>BASIC</b>.
<p>
	<img src="img/6.png"/>
</p>
* En el archivo <b>sun-web.xml</b> de nuestro proyecto damos de alta a nuestro rol y al grupo al que pertenece.
<p>
	<img src="img/7.png"/>
</p>
* En el archivo <b>glassfish-web.xml</b> de nuestro proyecto hacemos lo mismo que en el archivo de configuración anterior utilizando la etiqueta "security-role-mapping".
<p>
	<img src="img/8.png"/>
</p>
* Recompilamos el proyecto y lo subimos al servidor Glassfish, de ahora en adelante nos pedirá un usuario y contraseña (en este caso ocupamos el que dimos de alta que es el usuario <b>equipo7</b> con la contraseña <b>equipo7</b>).
<p>
	<img src="img/9.png"/>
</p>
* Como el usuario y la contraseña son válidas nos da acceso al sitio.
<p>
	<img src="img/10.png"/>
</p>
* En caso de que no sea así nos mandará otra vez a la siguiente pantalla:
<p>
	<img src="img/9.png"/>
</p>
* Si presionamos cancelar o escape, nos manda a la siguiente pantalla:
<p>
	<img src="img/11.png"/>
</p>
