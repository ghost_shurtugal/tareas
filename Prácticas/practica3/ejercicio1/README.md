Ejercicio 1
----------------------------------------------------

## Esquema de intercambio de llaves (propuesto por Diffie Hellman)

Este sistema es propuesto por Diffie y Hellman debido a que fueron conscientes del mayor problema de privacidad, la extracción de información de una comunicación a través de canales inseguros. 

El sistema de distribución de llaves públicas ofrece un acercamiento diferente para eliminar la necesidad de un canal de distribución de llaves seguras. Para ello cada interlocutor del mensaje elige dos números públicos y un número secreto. La idea de Diffie y Hellman es que es fácil calcular las potencias de módulo a un primo, pero es difícil revertir el proceso: si alguien pregunta qué potencia de 2 módulo 11 es 7, tendría que experimentar un poco para responder, aunque 11 es un pequeño primo. Si usas un enorme primo en lugar de otro, entonces esto se convierte en un problema muy difícil incluso en una computadora.

A continuación ambos interlocutores utilizan por separado una fórmula matemática que combina los dos números transformados con su número secreto y al final los dos llegan al mismo número resultado, que será la clave compartida.

Para ilustrar esta explicación haremos uso del clásico ejemplo de Alicia y Bob.
Alicia y Bob, usando una comunicación insegura, acuerdan un gran p primo y un generador g. A ellos nos les importa si los escuchan. 
Alicia elige un entero aleatorio grande x<sub>A</sub> &lt; p y lo mantiene en secreto. Por otro lado, bob elige x<sub>B</sub> &lt;p y también lo mantiene en secreto. Estas serán sus llaves privadas. 

Alicia computa su llave pública y<sub>A</sub> &equiv; g<sup>xA</sup> (módulo p) y la envía Bob usando comunicación insegura. Bob computa su llave pública y<sub>B</sub> &equiv; g<sup>xB</sup> y la envía a Alicia. Aquí 0 &lt;y<sub>A</sub>&lt;p,0&lt;y<sub>B</sub>&lt;p.

Alicia computa z<sub>A</sub> &equiv; y<sub>B</sub><sup>xA</sup> (módulo p) y Bob computa  zB  yAx B (módulo p). Aquí zA&lt;p,zB&lt;p.
Pero z<sub>A</sub> = z<sub>B</sub>, desde z<sub>A</sub> &equiv; y<sub>B</sub><sup>xA</sup> &equiv; (g<sup>xB</sup> )<sup>xA</sup> = g<sup>(x A xB)</sup> (módulo p) y de manera similar z<sub>B</sub> &equiv; (g<sup>xA</sup>)<sup>xB</sup> = g<sup>(xA xB)</sup> (módulo p). Entonces este valor es su llave secreta compartida. Pueden usarlo para cifrar y descifrar el resto de sus comunicaciones con un método más rápido.

Estos procesos están sujetos a una tercera persona que esté “escuchando” la conversación, en este caso llamado Mallory. Este puede realizar dos tipos de ataque:

* Un ataque pasivo: Si Mallory poseyera p, g, A y B, podría calcular el secreto compartido si tuviera también uno de los valores privados (a o b). Obtener a o b a partir de A o B invirtiendo la función (a=log disc<sub>p</sub> (A) y b= log disc<sub>p</sub> (B)) es el problema del logaritomo discreto en Z*<sub>p</sub>, un problema que se cree intratable computacionalmente siempre que p sea un número primo grande de 200 o más dígitos que no cumplan ciertas  características debilitantes. 

* El protocolo es sensible a ataques activos del tipo Man-in-the-middle. Si la comunicación es interceptada por un tercero, éste se puede hacer pasar por el emisor cara al destinatario y viceversa, ya que no se dispone de ningún mecanismo para validar la identidad de los participantes en la comunicación. Así, el "hombre en el medio" podría acordar una clave con cada participante y retransmitir los datos entre ellos, escuchando la conversación en ambos sentidos. Una vez establecida la comunicación simétrica, el atacante tiene que seguir en medio interceptando y modificando el tráfico para que no se den cuenta. Observar que para que el ataque sea operativo, el atacante tiene que conocer el método de cifrado simétrico que será utilizado. Basarse en la ocultación de algoritmo simétrico de cifrado no cumple con los principios de Kerckhoffs (la efectividad del sistema no debe depender de que su diseño permanezca en secreto).

## Recursos
* New Directions in Cryptography
  - <a href="https://ee.stanford.edu/~hellman/publications/24.pdf">Enlace 1</a>



