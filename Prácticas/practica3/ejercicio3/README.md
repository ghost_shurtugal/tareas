Ejercicio 3
----------------------------------------------------

## 1. Certificados

* Primero se ubicó donde se encontraba el archivo CA.pl para generar los certificados.

<p align="center">
  <img src="img/1.png"/>
</p>

* Utilizando el comando encontrado se generó el certificado (CA -newca).

<p align="center">
  <img src="img/2.png"/><br/>
  <img src="img/3.png"/>
</p>

* Posteriormente se generó la private key (CA -newreq) y se firma una vez creada (CA -sign).

<p align="center">
  <img src="img/4.png"/><br/>
  <img src="img/5.png"/><br/>
  <img src="img/6.png"/><br/>
  <img src="img/7.png"/>
</p>

* Para que apache pueda aceptar ssl instalamos mod_ssl

<p align="center">
  <img src="img/8.png"/>

</p>

* Editamos los datos del host y las rutas de los certificados y llaves.

<p align="center">
  <img src="img/9-1.png"/><br/>
  <img src="img/9-2.png"/>
</p>

* Apartir de el archivo .pem generamos el archivo .crt para que el apache lo pueda identificar. También copiamos los certificados a '/etc/pki/tls/certs' y reiniciamos el servicio con el password ocupado en la creación de los certificados y llaves.

<p align="center">
  <img src="img/10.png"/>
</p>

* Entramos al navegador y podemos observar que el certificado tuvo efecto al hacer conexión por https.

<p align="center">
  <img src="img/11.png"/><br/>
  <img src="img/12.png"/>
</p>
