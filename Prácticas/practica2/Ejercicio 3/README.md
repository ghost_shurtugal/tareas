Ejercicio 3
----------------------------------------------------

## 1. Instalando servidor dns

* Instalamos el servicio con el comando 'yum -y install bind':

<p align="center">
  <img src="img/1.png"/><br/>
  <img src="img/2.png"/>
</p>

* Acto seguido añadimos la url en el archivo '/etc/hosts' con la linea:
	127.0.0.1   redes2018.equipo7.mx
<p align="center">
  <img src="img/3.png"/>
</p>
