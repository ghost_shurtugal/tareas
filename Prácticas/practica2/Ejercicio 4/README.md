Ejercicio 4
----------------------------------------------------

## Preguntas

* Se resuevle a través del servidor dns buscando únicamente en la red local.

* Un root name server, es decir, un servidor raíz de nombres de dominio (también conocido como DNS root server o root server por su nomenclatura en inglés) es un servidor que desempeña una función esencial en lo relativo a la traducción de los nombres de dominio en direcciones IP: este da respuesta a las solicitudes de los clientes (requests) en la zona raíz del sistema de nombres de dominio (la zona raíz señala el nivel más alto en el espacio de nombres del DNS). 


* HTTP soporta los siguientes métodos para solicitar información:
 - El método GET  solicita una representación de un recurso específico. Las peticiones que usan el método GET sólo deben recuperar datos.
 - El método HEAD pide una respuesta idéntica a la de una petición GET, pero sin el cuerpo de la respuesta.
 - El método CONNECT establece un tunel hacia el servidor identificado por el recurso.

## Recursos
* DNS Root server:
  - <a href="https://www.1and1.mx/digitalguide/servidores/know-how/que-es-un-root-server/">Enlace 1</a>

* HTTP methods:
  - <a href="https://developer.mozilla.org/es/docs/Web/HTTP/Methods">Enlace 2</a>

