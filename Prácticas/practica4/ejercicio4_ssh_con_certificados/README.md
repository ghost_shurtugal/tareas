Ejercicio 4
----------------------------------------------------

## 1. Conectándose con ssh con certificado

* Se utiliza el comando <b>ssh-keygen -t dsa</b>, dejamos el passphrase en blanco para que no pregunte alguna contraseña al usar el certificado y lo almacenamos el la lozalización default.

* Deamos permisos de sólo lectura para el usuario actual y posteriormente le mandamos al servidor host el certificado para que lo reconozca.

* Utilizamos el password y el username para que nos de acceso de hacer esta operación y nos responde que se añadió la key.

* Probamos que esto fue un éxito al ejecutar sólo el comando <b>ssh root@10.0.2.15 -p 22</b>, como tenemos ya el certificado validado se autentifica con él y nos da acceso al servicio sin necesidad de utilizar contraseña.

<p align="center">
  <img src="img/1.png"/>
</p>
