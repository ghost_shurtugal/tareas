Ejercicio 1
----------------------------------------------------

## OpenSSH
* Primero instalamos las paqueterias necesarias para levantar el servicio;
<p>
	<img src="img/1.png"/>
</p>
 En este caso ya se tenían instaladas.

* Iniciamos el servicio, posteriormente iniciamos sesión vía ssh al localhost primero aceptando el amacenar la fingerprint del servidor y continuamos ingresando la contraseña.
Dentro de la sesión hacemos un echo con la palabra "HOLA".
<p>
	<img src="img/2.png"/>
</p>
En otra consola observamos con el comando ss que hay actividad del servicio ssh en el puerto 22.
<p>
	<img src="img/3.png"/>
</p>


