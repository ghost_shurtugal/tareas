Ejercicio 1
----------------------------------------------------

## Instalando el servicio SMTP y probando
* Primero instalamos las paqueterias necesarias para levantar el servicio;
<p>
	<img src="img/1.png"/>
</p>
 En este caso ya se tenían instaladas.

* Posteriormente se edita el archivo <b>/etc/postfix/main.cf</b> para añadir las siguientes lineas que bloquean el Open Relay:
<p>
	<img src="img/2.png"/>
</p>
* También añadimos la variable <b>mydomain</b> con el valor de <b>ciencias.redes.io</b> para definir el nombre del dominio.
<p>
	<img src="img/3.png"/>
</p>
* Finalmente reiniciamos el servicio SMTP.
<p>
	<img src="img/4.png"/>
</p>
* Mandamos un correo a <b>fcastaneda@ciencias.unam.mx</b> para ver si al configuración fue correcta con el sig. comando:<br/>
<b>echo "Cuerpo del correo" | mail -s "Correo de prueba" fcastaneda@ciencias.unam.mx</b>, se aprecia en la cola antes de enviar:
<p>
	<img src="img/5.png"/>
</p>
