Ejercicio 3
----------------------------------------------------

## Funcionamiento de SMTP
* Para el caso de mandar un correo desde un MUA sender a un MUA recipient.
<p align="center">
	<img src="img/1.png"/> 	<img src="img/1.jpg"/>
</p>
1. El usuario MUA (Mail User Agent) se identifica.
2. Una vez identificado mandar un correo y se queda en la píla de envío.
3. El servicio smtp revisa periódicamente la pila de enviados y en un tiempo <b>t</b> el MTA (Mail Trasnfer Agent) lo envía.
4. Este correo puede pasar por varios MTA antes de llegar la MTA receptor, aunque se acalara que antes de pasar al MTA receptor es posible que pase por un filtro de spam y de malware.
5. Después de esta validación se deja en una caché de entrada que periódicamente redirige a la bandeja de entrada del usuario destinatario o receptor.
6. Se manda a la la bandeja de entrada del usuario receptor y este por medio de otro servicio como pop3 o IMAP puede acceder a este correo.
