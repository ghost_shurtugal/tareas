Ejercicio 4
----------------------------------------------------

## SMTPS
* Instalamos las paqueterías necesarias para el servicio (en este caso ya se encuentran instaladas).
<p>
	<img src="img/1.png"/>
</p>

* Generamos un certificado como con FTPS para que nos permita añadirle TLS al servicio SMTP.
<p>
	<img src="img/2.png"/>
</p>
* En el archivo <b>/etc/postfix/main.cf</b>, añadimos las variables para que postfix configure el servicio SMTPS a partir de las localizaciones de los certificados, entre otras configuraciones relevantes.
<p>
	<img src="img/3.png"/>
</p>
* Por último reiniciamos el servicio de postfix y vemos que este funciona sin problema alguno ahora con SMTPS.
<p>
	<img src="img/4.png"/>
</p>
