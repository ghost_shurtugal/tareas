Ejercicio 1
----------------------------------------------------

## Instalando y configurando FTP
* Primero instalamos las paqueterias necesarias para levantar el servicio;
<p>
	<img src="img/1.png"/>
</p>

* Habilitamos e iniciamos el servicio, posteriormente vemos su status.
<p>
	<img src="img/2.png"/>
</p>

* Al intentar iniciar como usuario anónimo vemos que es permitido:
<p>
	<img src="img/3.png"/>
</p>

* Configuramos el servicio para bloquear las conexiones anónimas y posteriormente reiniciamos el servicio para que los cambios se reflejen.<br/>
(En lugar de que tenga valor 'YES' el atributo de configuración 'anonymus_enable' lo cambiamos a 'NO'):
<p>
	<img src="img/4.png"/><br/>
	<img src="img/5.png"/>
</p>

* Volvemos a intentar iniciar sesión como usuario anónimo pero esta vez vemos que se rechaza la conexión:
<p>
	<img src="img/6.png"/>
</p>

