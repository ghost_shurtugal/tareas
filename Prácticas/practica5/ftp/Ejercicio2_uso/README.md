Ejercicio 2
----------------------------------------------------

## Uso de FTP
* En la siguiente captura podemos apreciar el funcionamiento del servidor ftp haciendo las siguientes operaciones:
 -  Creación de un directorio en el servidor (mkdir).
 -  Cambiar de directorio en el servidor (cd).
 -  Copiar archivo del cliente al servidor (put).
 -  Listar contenidos de un directorio del servidor (ls).
 -  Eliminar archivo del servidor (delete).
 -  Eliminar directorio del servidor (rmdir).
<p>
	<img src="img/1.png"/>
</p>
