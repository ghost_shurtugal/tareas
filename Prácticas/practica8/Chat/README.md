TCP
----------------------------------------------------

* Se hizo un programa de cliente y otro de servidor como se describió en la especificación de la práctica (se utilizó netbeans al momento de desarrollar, el cliente tiene una interfaz gráfica y se anexan dos zips con los proyectos).

* El programa del servidor funciona con sólo ejecutarlo, el cliente necesita dos parámetros:
 - El primero es la ip del servidor.
 - El segundo es el username del cliente.
<br/>
En caso de no tener este formato no iniciará.


Preguntas
-------------------------------------------------------
1. ¿Enlista la clasificación de los puertos y los rangos?
 - El puerto 0 es un puerto reservado, pero es un puerto permitido si el emisor no permite respuestas del receptor.
 - Los puertos 1 a 1023 reciben el nombre de Puertos bien conocidos.
 - Los puertos 1024 a 49151 son los llamados Puertos registrados, y son los de libre utilización.
 - Los puertos del 49152 al 65535 son Puertos efímeros, de tipo temporal, y se utilizan sobre todo por los clientes al conectar con el servidor.

2. De acuerdo a la cabecera IP, describir la razón de la existencia de 65535 puertos.
 - En la cabecera el puerto se almacena en un espacio de de 16 bits, lo que permite rangos que van desde 0 a 65535 (2<sup>16</sup>).

3. Diferencia entre un socket IPv4 e IPv6
 - En IPv4 estas API son la forma en que las aplicaciones utilizan TCP/IP. Las aplicaciones que no necesitan IPv6 no se ven afectadas por los cambios de sockets para dar soporte a IPv6.
 - En IPv6 mejora los sockets de forma que ahora las aplicaciones pueden utilizar IPv6, con una familia de direcciones nueva: AF_INET6. <br/>Las mejoras han sido diseñadas de forma que las aplicaciones IPv4 existentes no se ven afectadas de ningún modo por IPv6 y los cambios de API. Se da cabida a las aplicaciones que quieren dar soporte al tráfico IPv4 e IPv6 concurrente, o sólo al tráfico IPv6, utilizando direcciones IPv6 correlacionadas con IPv4 con el formato ::ffff:a.b.c.d, donde a.b.c.d es la dirección IPv4 del cliente. <br/>Las nuevas API también incluyen soporte para la conversión de direcciones IPv6 de texto a binario y de binario a texto.

4. Ejemplos de sockets como mecanismo IPC dentro de un mismo sistema operativo.<br/>
  * Stream socket: Crea un canal virtual de comunicación sobre streams.
  * Datagram socket:  No asegura el orden de preservación, pérdida y duplicación de mensajes
  * Sequential packet socket: Provee conexiones confliables para datagramas de una longitud máxima fija. No hay protocolo implementado para este tipo.
  * Raw socket: Provee acceso a protocolos subyacentes de comunicación.
