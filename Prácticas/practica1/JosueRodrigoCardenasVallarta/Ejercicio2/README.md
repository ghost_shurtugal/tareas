Ejercicio 2
----------------------------------------------------

## Documentación

/**
* @file practica1.c
* @brief Este archivo contiene el ejercicio 2 de la práctica 1.
*
* @author Josué Rodrigo Cárdenas Vallarta
*
* @date 14/02/2018
*/

## void printDirection(char* elementos, int longitudA)

/**
 * @brief Función que imprime elementos de un arreglo de caracteres junto con su dirección en memoria.
 *
 * @param elementos Es el apuntador del arreglo de caracteres a imprimir.
 * @param longitudA Es el tamaño del arreglo.
*/

## void swap(char* c1, char* c2)
/**
 * @brief Función que hace un swap de valores tipo caracter entre dos apuntdores de memoria.
 *
 * @param c1 Es el apuntador del caracter uno.
 * @param c2 Es el apuntador del caracter dos.
*/

## int particion(char* elementos, int desde, int hasta)
/**
 * @brief Función que genera la partición para el algoritmo de quickSort y devuelve el índice
 *			del elemento pivote.
 *
 * @param elementos Es el apuntador del arreglo de caracteres.
 * @param desde Es el punto inicial de la partición.
 * @param hasta Es el punto final de la partición.
 *
 * @return El índice del elemento pivote.
 */

## void quickSort(char* elementos, int desde, int hasta)
/**
 * @brief Función que implementa el algoritmo de quicksort, no genera un nuevo arreglo si no
 * 			que opera en el mismo arreglo que se le pasa como parámetro.
 *
 * @param elementos Es el apuntador del arreglo de caracteres con el que operaremos.
 * @param desde Es el índice inicial para hacer el algoritmo en el arreglo.
 * @param hasta Es el índice final de la región de elementos a aplicar con el algoritmo.
 */





