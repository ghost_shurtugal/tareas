Ejercicio 4
----------------------------------------------------

## Diferencias entre dispositivos de bloques y de red
* Dispositivo de bloques:
  - Un dispositivo de bloques es un componente de la computadora en el que los datos se transmiten en conjuntos en la comunicación con la unidad central de proceso.

* Dispositivo de red:
  - Un dispositivo de red es aquel que se conecta de forma directa a un segmento de red.

* Diferencia:
  - La diferencia radica en que los de bloques sólo que comunican en un equipo de cómputo mientras que los de red permiten la conexión entre distintos equipos de cómputo.

## Ejemplos de lectura de dispositivos de red en lenguaje C

* Leer redes wifi
  - En la siguiente referencia se puede observar como con la biblioteca 'linux/wireless.h' se puede listar las redes wifi disponibles.
    - <a href="https://www.linuxquestions.org/questions/linux-networking-3/c-program-source-details-required-for-scan-the-available-wireless-network-863276/">Leer redes wifi</a> 

* Mandar un stream de caracteres por medio de Sockets
  - En el siguiente código se puede ver como por medio de un socket y por medio del protocolo TCP se manda un stream de caracteres entre equipos de cómputo.
    - <a href="https://stackoverflow.com/questions/21433205/ethernet-frame-transmission-via-raw-socket">Transferencia por sockets</a>


## Recursos
* Dispositivo de bloques:
  - <a href="https://es.wikipedia.org/wiki/Dispositivo_de_bloques">Enlace 1</a>

* Dispositivos de red:
  - <a href="https://es.slideshare.net/mjyadira/dispositivos-de-red-9383598">Enlace 2</a>

* Wireless Tools for Linux
  - <a href="https://hewlettpackard.github.io/wireless-tools/Tools.html">Enlace 3</a>
