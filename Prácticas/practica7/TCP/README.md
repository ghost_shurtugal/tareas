TCP
----------------------------------------------------

* Se hizo un programa de cliente y otro de servidor como se describió en la especificación de la práctica (se utilizó netbeans al momento de desarrollar y se anexan dos zips con los proyectos).

* Se hizo una captura de TCP para observar el Three Way Handshake (se anexa archivo) y hacemos énfasis en los primeros tres paquetes:
 1. 11:50:02.407158 IP localhost.35402 > localhost.ircu-2: Flags <b>[S]</b>, seq 1733223532, win 43690, options [mss 65495,sackOK,TS val 3370134120 ecr 0,nop,wscale 7], length 0
 2. 11:50:02.407213 IP localhost.ircu-2 > localhost.35402: Flags <b>[S.]</b>, seq 2553073188, ack 1733223533, win 43690, options [mss 65495,sackOK,TS val 3370134120 ecr 3370134120,nop,wscale 7], length 0
 3. 11:50:02.407246 IP localhost.35402 > localhost.ircu-2: Flags <b>[.]</b>, ack 1, win 342, options [nop,nop,TS val 3370134120 ecr 3370134120], length 0

 Se puede apreciar que en el primer paquete el cliente le manda una petición de syncronia al server, en el segundo paquete el server rersponde con un SYN junto con un ACK y el cliente termina el Three Way Handshake con un ACK en el paquete número tres.<br/>
 Los siguientes paquetes mandan los bytes de la imagen.

