Tarea de SSL
----------------------------------------------------

## Nombre

Cárdenas Vallarta Josué Rodrigo

### Correo electrónico

rodrigo.cardns@gmail.com

### Nota

En el ejercio para juntar los certificados sólo se copió el contenido del archivo <b>certificate-01.pem</b> en <b>chain.pem</b>, esto debido a que sólo se obtuvo un certificado que no era el del servidor.

### Ejercicio 4

* Para validar un certificado es necesario comparar el certificado con la lista de certificados conocidos y emitidos por una autoridad certificadora.
* <b>/etc/ssl/certs</b> es el directorio en donde se almacenan los certificados confliables.
* El archivo <b>chain.pem</b> contiene todos los certificados que requiere el certificado del servidor para ser validado, es por ello que es importante al agregarlo a este comando.
